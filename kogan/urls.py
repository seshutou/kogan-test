from django.conf.urls import include, url
from rest_framework import routers

from kogan import views

router = routers.SimpleRouter()
router.register(r'category', views.CategoryViewset, base_name='category')

urlpatterns = [
    url(r'^', include(router.urls)),
]
