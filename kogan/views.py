# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from rest_framework.decorators import action
# Create your views here.
from rest_framework.response import Response

from kogan.utils import KoganFactory


class CategoryViewset(viewsets.ViewSet):

    permission_classes = []
    lookup_field = 'category'

    @action(detail=True, url_path='average_cubic_weight', url_name='average_cubic_weight')
    def get_average_cubic_weight(self, request, category=None, *args, **kwargs):
        avg_cubic_weight, result_list = KoganFactory.get_average_weight_for_a_category(category)
        context = {
            'avg_cubic_weight': avg_cubic_weight,
            'result_list' : result_list
        }
        return Response(data=context)

